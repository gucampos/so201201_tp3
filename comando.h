#ifndef COMANDO
#define COMANDO 1

#define EMPTY_STRING ""

/*
 * comando_t eh uma struct que representa um comando a ser
 * executado no sistema.
 *
 */
typedef struct {

    char *cmd;          // o caminho para o arquivo a ser executado
    char *inputfile;    // se houver, o caminho para arquivo de entrada
    char *outputfile;   // se houver, o caminho apra arquivo de saida

    char **argv;        // vetor de argumentos para o comando

    int  argc;          // contagem da quantidade de argumentos

    int hasinput;       // tem input?
    int hasoutput;       // tem output?
} comando_t;

int comando_new(comando_t *comando, char *cmdline);
int comando_delete(comando_t *cmd);
void comando_print(comando_t *cmd);
void alloc_comando_t(comando_t *cmd);
void free_comando_t(comando_t *cmd);
#endif
