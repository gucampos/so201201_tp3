SHELL = /bin/bash

CC = /usr/bin/gcc
FL = -g -Wall
TS = testes
VALGRIND=/usr/bin/valgrind --leak-check=full --show-reachable=yes --track-origins=yes

tp3shell: tp3shell.o manager.o comando.o shell.o
	${CC} ${FL} -o $@ $^

tp3shell.o: tp3shell.c
	${CC} ${FL} -c -o $@ $^

manager.o: manager.c manager.h comando.o shell.o debug.h
	${CC} ${FL} -c -o $@ $<

shell.o: shell.c shell.h comando.o debug.h
	${CC} ${FL} -c -o $@ $<

comando.o: comando.c comando.h
	${CC} ${FL} -c -o $@ $<

valgrind: tp3shell
	${VALGRIND} ./$< testes/echo.sh
	${VALGRIND} ./$< testes/ls.sh
	${VALGRIND} ./$< testes/pipes.sh

test: tp3shell
#	./$< testes/echo.sh
#	./$< testes/ls.sh
	./$< testes/pipes.sh

clean: 
	rm *.o tp3shell

.PHONY: clean
