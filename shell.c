#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>

#include "comando.h"
#include "shell.h"

int run_command(comando_t *comando)
{

    int pipe_in[2];
    int pipe_out[2];

    pid_t worker_ps, reader_ps, writer_ps;

    pipe(pipe_in);
    pipe(pipe_out);

    worker_ps = reader_ps = writer_ps = 1;

    if(DEBUG) printf("1) ME: %6d, WRITER: %6d, WORKER: %6d, READER: %6d\n",
            getpid(), writer_ps, worker_ps, reader_ps);

    /*
     * FATHER
     * forked_by:   NONE
     * forks:       WRITER
     */
    writer_ps = fork();

    /*
     * FATHER: esperando writer 
     */
    if(writer_ps!=0) {
        /*
         * O Pai nao precisa de nenhum pipe
         */
        close(pipe_in[READ_END]);
        close(pipe_in[WRITE_END]);
        close(pipe_out[READ_END]);
        close(pipe_out[WRITE_END]);
        /*
         * Aguardamos pelo escritor e tratamos erros
         */
        if (waitpid(writer_ps, NULL, WUNTRACED) == -1) {
            perror("FATHER: esperando WRITER");
        }

        return FATHER;
    }

    if(DEBUG) printf("2) ME: %6d, WRITER: %6d, WORKER: %6d, READER: %6d\n",
            getpid(), writer_ps, worker_ps, reader_ps);

    /*
     * WRITER
     * forked_by:   FATHER
     * forks:       WORKER
     */
    if (writer_ps == 0) worker_ps = fork();

    if(DEBUG) printf("3) ME: %6d, WRITER: %6d, WORKER: %6d, READER: %6d\n",
            getpid(), writer_ps, worker_ps, reader_ps);

    /*
     * WRITER: esperando worker
     */
    if ( (writer_ps==0) && (worker_ps!=0) ) {
        /*
         * Fechamos as pontas dos pipes que esse processo nao usa
         */
        close(pipe_out[WRITE_END]);
        close(pipe_in[WRITE_END]);
        close(pipe_in[READ_END]);
        /*
         * Aguardamos pelo executor e tratamos erros
         */
        if (waitpid(worker_ps, NULL, WUNTRACED) == -1) {
            perror("WRITER: esperando WORKER");
        }
        /*
         *  Se worker mudou estado e foi requisitado output, inicia escrevendo
         * dados no pipe. Tratamento de erro incluso
         */
        if (comando->hasoutput) {
            /*
             * Delega a escrita para a funcao especifica
             */
            if (writer_thread(pipe_out[READ_END], comando)!=0) {
                perror("WRITER: retorno da escrita");
            }
        }
        close(pipe_out[READ_END]);

        return WRITER;
    }

    /*
     * WORKER
     * forked_by:   WRITER
     * forks:       READER
     */
    if (worker_ps == 0) reader_ps = fork();

    /*
     * WORKER: esperando reader
     */
    if ( (worker_ps==0) && (reader_ps!=0) ) {
        /*
         * Conectamos os pipes nas suas pontas se necessario
         */
        if (comando->hasinput) dup2(pipe_in[READ_END], fileno(stdin));
        if (comando->hasoutput) dup2(pipe_out[WRITE_END], fileno(stdout));
        /*
         * Fechamos as pontas dos pipes que esse processo nao usa (todos ^^)
         */
        close(pipe_out[READ_END]);
        close(pipe_out[WRITE_END]);
        close(pipe_in[READ_END]);
        close(pipe_in[WRITE_END]);
        /*
         *  Aguardamos pelo leitor e tratamos erros
         */
        if (waitpid(reader_ps, NULL, WUNTRACED) == -1) {
            perror("WORKER: esperando READER");
        }
        /*
         * Delega a escrita para a funcao especifica
         */
        if (worker_thread(pipe_in[READ_END], pipe_out[WRITE_END],comando)!=0) {
            perror("WORKER: retorno de execucao");
        }

        return WORKER;
    }
        
    if(DEBUG) printf("4) ME: %6d, WRITER: %6d, WORKER: %6d, READER: %6d\n",
            getpid(), writer_ps, worker_ps, reader_ps);

    /*
     * READER
     * forked_by:   WORKER
     * forks:       NONE
     */

    /*
     * READER: esperando ninguem
     */
    if (reader_ps==0) {
            /*
         * Fechamos as pontas dos pipes que esse processo nao usa
         */
        close(pipe_out[READ_END]);
        close(pipe_out[WRITE_END]);
        close(pipe_in[READ_END]);

        /*
         *  Se foi requisitado input redirection, iniciamos a coleta de dados
         * do arquivo. Caso contrario, nada eh escrito no pipe
         */
        if (comando->hasinput) {
            /*
             * Delega a leitura para a funcao especifica
             */
            if (reader_thread(pipe_in[WRITE_END], comando)!=0) {
                perror("READER: retorno de leitura");
            }
        }
        close(pipe_in[WRITE_END]);
        
        return READER;
    }

    /*
     * Se ninguem retornou, deu bziu
     */
    return -1;
}

/*
 * A funcao apenas executa o comando e trata erros
 */
int worker_thread(int fd_read, int fd_write, comando_t *comando)
{   
    
    if (DEBUG) puts("WORKER THREAD IN"); //DEBUG

    execvp(comando->cmd, comando->argv); 

    /*
     * Se chegou ate aqui o exec falhou
     */
    perror("WORKER: execvp");
    exit(1);
} 

/*
 * A funcao le tudo do arquivo passado e joga no file descriptor, nada mais
 */
int reader_thread(int fd_write, comando_t *comando)
{
    if (DEBUG) puts("READER THREAD IN"); //DEBUG

    char *read_buffer = calloc(TAMANHO_LINHA, sizeof(char));
    FILE *input = fopen(comando->inputfile, "r+");
    int readsize = 0;
    int writesize = 0;

    do {

        if(DEBUG) puts("READER: starting");

        readsize = fread(read_buffer, TAMANHO_LINHA, 1, input);
        writesize = write(fd_write, read_buffer, TAMANHO_LINHA);

        if (writesize<=0) perror("READER: escrevendo no pipe");

        if (DEBUG) printf("READER: wrote %d to pipe\n", writesize);

   } while (readsize<0);

    fclose(input);
    free(read_buffer);

    if (DEBUG) puts("READER THREAD OUT"); //DEBUG
    
    return 0;
}

/*
 * A funcao le tudo do file descriptor e joga no arquivo, nada mais
 */
int writer_thread(int fd_read, comando_t *comando)
{
    if (DEBUG) puts("WRITER THREAD IN"); //DEBUG

    char *write_buffer = calloc(TAMANHO_LINHA, sizeof(char));
    FILE *output = fopen(comando->outputfile, "w+");
    int readsize = 0;

    if (output==NULL) perror("WRITER: abrindo arquivo");

    do {
        /*
         * Le do pipe
         */
        readsize = read(fd_read, write_buffer, TAMANHO_LINHA);

        //if (DEBUG) fputs("DEBUG IN", output);
        /* 
         * Escreve no arquivo
         */
        if (fwrite(write_buffer, TAMANHO_LINHA, 1, output)<0) {
        //if (fputs(write_buffer, output)<0) {
            perror("WRITER: escrevendo no arquivo");
        }

        //if (DEBUG) fputs("DEBUG OUT", output);

        fflush(output);

    } while (readsize>=TAMANHO_LINHA);

    fclose(output);
    free(write_buffer);

    if (DEBUG) puts("WRITER THREAD OUT"); // DEBUG

    return 0;
}
