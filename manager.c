#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#include "comando.h"
#include "shell.h"
#include "manager.h"

int ioloop(FILE *input, char mode)
{


    comando_t comando;

    char *linha = malloc(1024 * sizeof(char));
    int child = 0;

    do {

        if(DEBUG) printf("================================================\n"); //DEBUG

        /*
         * Lemos linha a linha de entrada e caso a linha contenha comandos
         * vamos 'forkar' e executar esses comandos
         */
        if(mode=='i') printf(SHELL_TEXT); //textinho do shell

        linha = fgets(linha, TAMANHO_LINHA, input);

        if(!fimdoshell(linha)) {

            // Instancia um comando, se retornar -1, nao tem comando
            if(comando_new(&comando,linha)==-1) continue;

            //if (DEBUG) comando_print(&comando); //DEBUG

            // Delega o comando para o shell
            child = run_command(&comando);

            if(child!=0) break;

        } else break;
    } while (!fimdoshell(linha));

    if (DEBUG) printf("IOLOOP: Fim do ioloop para %d\n",getpid());

    // cleanup
    comando_delete(&comando);
    free(linha);
    fclose(input);

    if (DEBUG) printf("IOLOOP: Return do ioloop para %d\n",getpid());

    return 0;
}

int fimdoshell(char *comando)
{
    // plain and simple: se for algum fim de entrada, retorna true
    return  (NULL == comando) ||
            ! strcmp("EOF\n",comando) ||
            ! strcmp("FIM\n",comando) ||
            ! strcmp("fim\n",comando);
}
