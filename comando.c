#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comando.h"
int comando_new(comando_t *comando, char *cmdline)
{
    alloc_comando_t(comando);

    // Faz strip nos line-breaks (pra melhorar o output)
    if(cmdline[strlen(cmdline)-1] == '\n') {
        cmdline[strlen(cmdline)-1] = '\0';
    }

    /*
     * Caracteres reservados do shell
     */
    const char *del     = " ";
    const char *pipein  = "<=";
    const char *pipeout = "=>";

    /*
     * Vamos pro loop de filtro da string
     */
    char *tok = NULL;
    int counter = 0;

    comando->hasinput = 0;
    comando->hasoutput=0;

    do {
        // Se for a primeira token, precisamos citar a string
        if (counter == 0) {
            tok = strtok(cmdline, del);

            // Se nao tiver o que ler vamos embora
            if(NULL==tok) return -1;

            // Se for linha em branco, vamos embora
            if(!strcmp("\n",tok)) return -1;

            // Se a token eh comentário, continamos
            if(tok[0] == '#') return -1;

            // Se ha uma token, eh o nome do arquivo
            strncpy(comando->cmd,tok,strlen(tok));

            // Incrementemos, daqui pra frente o tratamento eh outro
            counter++;
        }

        else tok = strtok(NULL,del);

        // Se a token eh nula ja podemos sair do loop aqui
        if (NULL==tok) break;

        // Se a token eh linha em branco, porem, continuamos
        if (!strcmp("\n",tok)) continue;

        counter++;

        // Testamos por pipe input
        if (!strcmp(tok,pipein)) {
            // Se eh pipe input, o proximo argumento eh o arquivo de entrada
            tok = strtok(NULL,del);

            // Tratamento de erros e copia segura
            if(NULL==tok) {
               error(1,11,"Arquivo de entrada nao informado");
            } else {
                strncpy(comando->inputfile,tok,strlen(tok));
            }

            continue;
        }

        // Testamos por pipe output
        if (!strcmp(tok,pipeout)) {
            // Se eh pipe output, o proximo argumento eh o arquivo de saida
            tok = strtok(NULL,del);

            // Tratamento de erros e copia segura
            if(NULL==tok) {
                error(1,12,"Arquivo de saida nao informado");
            } else {
                strncpy(comando->outputfile,tok,strlen(tok));
            }

            continue;
        }

        /*
         * Na ausencia de pipes, cada token eh um argumento:
         * Note que o fluxo soh chega aqui no caso de argumentos
         */
        comando->argv[comando->argc] = tok;
        comando->argc++;

    } while (NULL != tok);

    /*
     * preenche os valores de hasinput e hasoutput para uso futuro
     */
    if(strcmp(EMPTY_STRING, comando->inputfile)!=0) comando->hasinput=1;
    if(strcmp(EMPTY_STRING, comando->outputfile)!=0) comando->hasoutput=1;


    return 0;
}

int comando_delete(comando_t *cmd)
{
    free_comando_t(cmd);
    return 0;
}

void comando_print(comando_t *cmd)
{
    int i;
    printf("Comando:        %s\n",cmd->cmd);
    printf("Argumentos:     ");
    for(i=0;i<cmd->argc;i++) {
        printf("'%s', ",cmd->argv[i]);
    }
    printf("\n");
    printf("Input File:    '%s'\n",cmd->inputfile);
    printf("Output File:   '%s'\n",cmd->outputfile);
    puts("");
}

void alloc_comando_t(comando_t *cmd)
{
    cmd->argc       = 0;
    cmd->cmd        = calloc(1024,sizeof(char));
    cmd->argv       = calloc(1024,sizeof(char));
    cmd->inputfile  = calloc(1024,sizeof(char));
    cmd->outputfile = calloc(1024,sizeof(char));
}

void free_comando_t(comando_t *cmd)
{
    free(cmd->outputfile);
    free(cmd->inputfile);
    free(cmd->argv);
    free(cmd->cmd);
}
