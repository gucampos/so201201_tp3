#ifndef SHELL_H
#define SHELL_H 1

#include "comando.h"
#include "debug.h"

#define READ_END 0
#define WRITE_END 1
#define TAMANHO_LINHA 256

#define FATHER 0
#define READER 1
#define WORKER 2
#define WRITER 3

/*
 * Roda o comando em threads separadas para leitura e escrita, alem da execucao
 * retorna 0 caso esteja retornando do processo pai, e WORKER, READER ou WRITER
 * caso esteja retornando de alguma das children
 */
int run_command(comando_t *comando);

int worker_thread(int fd_read, int fd_write, comando_t *comando);
int reader_thread(int fd_write, comando_t *comando);
int writer_thread(int fd_read, comando_t *comando);
#endif
