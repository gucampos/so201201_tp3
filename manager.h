#ifndef MANAGER
#define MANAGER 1

#include "comando.h"
#include "debug.h"

#define SHELL_TEXT "tp3shell-gucampos-smiccoli$ "
#define TAMANHO_LINHA 256 


/*
 * Executa o comando pedido
 */
int ioloop(FILE *input, char mode);

/*
 * Retorna 1 se a linha contiver algum dos delimitadores de fim de arquivo:
 FIM
 fim
 EOF

 ou se chegamos ao fim do arquivo mesmo
 */
int fimdoshell(char *comando);

#endif
