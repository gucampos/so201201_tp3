#include <stdio.h>
#include <stdlib.h>
#include <error.h>

#include "manager.h"

int main( int argc, char **argv)
{

    FILE *tp3in = NULL;
    char MODE = 'b';

    /* Tratamento de erros */
    if (argc >= 3) {
        error(1, 10, "Numero incorreto de argumentos");
        exit(1);
    }

    /*
     *  Caso especificado arquivo de script, abre-o, em caso contrario
     *  conectamos a entrada padrao do sistema na nossa entrada padrao
     */
    if (argc == 2) {
        tp3in = fopen(argv[1],"r");
    } else {
        tp3in = stdin;
        MODE = 'i';
    }

    /* Tratamento de erros */
    if (NULL == tp3in) {
        perror("Erro na abertura de arquivo");
        exit(1);
    }

    /*
     *  Iniciamos aqui o loop que executara os comandos
     */
    ioloop(tp3in,MODE);

    return 0;
}
